// Sets the number of stars we wish to display
const numStars = 100;

// For every star we want to display
for (let i = 0; i < numStars; i++) {
  let star = document.createElement("div");  
  star.className = "star";
  const random = getRandomPosition();
  star.style.top = random[0] + 'px';
  star.style.left = random[1] + 'px';
  document.body.append(star);
}

// Gets random x, y values based on the size of the container
function getRandomPosition() {  
  const y = window.innerWidth;
  const x = window.innerHeight;
  const randomX = Math.floor(Math.random()*x);
  const randomY = Math.floor(Math.random()*y);
  return [randomX,randomY];
}




let req = new XMLHttpRequest();
let randomNumber = Math.floor((Math.random() *80) + 1);

let contentborder = document.querySelector('.contentborder');
let find = document.querySelector('.find');

const request = new XMLHttpRequest();

request.open("GET", "https://swapi.co/api/people/" + randomNumber, true);
request.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    
    contentborder.innerHTML += `
    <div class="personContent">
    <span>Name:</span>
    <h2>${data.name}</h2>
   
    <span>Height:</span>
    <p>${data.height}</p>
    
    <span>Weight:</span>
    <p>${data.mass} </p> 
    <
    <span>Birth year:</span>
    <p>${data.birth_year}</p>
  
    <span>Gender:</span>
    <p>${data.gender}</p>
    
    <div>
    `;
  } else {
    console.log("error błąd z połączeniem");
  }
}
request.send();

const HomePage = {
  template: `
    <div class="page HomePage">
      <h1>HomePage</h1>
    </div>
  `
};
const AboutPage = {
  template: `
    <div class="page AboutPage">
      <h1>AboutPage</h1>
    </div>
  `
};
const ContactPage = {
  template: `
    <div class="page ContactPage">
      <h1>ContactPage</h1>
    </div>
  `
};
const NotFoundPage = {
  template: `
    <div class="page NotFoundPage">
      <h1>404</h1>
    </div>
  `
};


const router = new VueRouter({
  routes: [
    { path: '/', component: HomePage },
    { path: '/about', component: AboutPage },
    { path: '/contact', component: ContactPage },
    { path: '*', component: NotFoundPage },
  ]
});

const vm = new Vue({
  el: "#app",
  router,
  data: {
  },
  computed: {
  },
  methods: {
  },
  mounted() {
  }
});